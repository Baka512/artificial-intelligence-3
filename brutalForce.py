from node import node

def start(n, MaxStep = 3):
        queue = []
        queue.append(n)
        for step in range(MaxStep + 1):
            print("\nСостояние: ", step)
            k=queue.__len__();
            while k>0:
                N = queue.pop(0)
                N.printSost()
                if N.isThanRequired():
                    if step==3: 
                        print("Найдено решение")
                        break
                else:
                    T =node(N.getSost(),0,1)
                    queue.append(T)
                    T =node(N.getSost(),1,2)
                    queue.append(T)
                    T =node(N.getSost(),0,2)
                    queue.append(T)
                k-=1
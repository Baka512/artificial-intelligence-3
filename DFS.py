from node import node

def start(n,  step = 0):
    print("\nШаг:", step, " ", end=' ')
    n.printSost()
    if ((n.isThanRequired() and (step == 3)) or (
        not (n.isThanRequired()) and  step  < 3 and ( 
           start(node(n.getSost(),0,1),step+1) or 
           start(node(n.getSost(),1,2),step+1) or
           start(node(n.getSost(),0,2),step+1)
        )
    )):
        return True
    else:
        return False
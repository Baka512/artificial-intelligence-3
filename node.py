class node:    
    S = dict();
    def __init__(self, s, i = 3, j = 3): 
        self.S = s
        if i!=3:
            self.S[i] = not (self.S[i])
            self.S[j] = not (self.S[j])
    
    def getSost(self):
        return self.S.copy()
    
    def isThanRequired(self):
        Q = self.S[0];
        for i in range(self.S.__len__()):
            if Q!=self.S[i]:
                return 0
        return 1
    
    def printSost(self):    
        print("(", end='')
        for i in range(self.S.__len__()):
            if self.S[i]:  
                print("  Орёл ", end='')
            else:
                print(" Решка ", end='')
        print(")  ", end=' ')